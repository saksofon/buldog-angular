import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public languages: any[];
  constructor() { }
  ngOnInit() {
    this.languages = [
      {link: '../assets/img/en.png'},
      {link: '../assets/img/pl.png'}
    ];

  }

}
