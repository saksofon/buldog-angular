import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public items: any[];
  constructor() { }
  ngOnInit() {
    this.items = [
      {name: 'HODOWLA BULDOGA Francuskiego "Nazwa"', url: 'galeria'},
      {name: 'Krzysztof Dmitrowski', url: 'galeria'},
      {name: '37-300 Leżajsk, ul Przemysłowa 3', url: 'szczenieta'}
    ];
  }
}
