import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public items: any[];
  public isActive: number;

  constructor() { }
  public change(index: number){
    this.isActive = index;
  }
  ngOnInit() {
    this.items = [
      {name: 'strona główna', url: 'main'},
      {name: 'nasze psy', url: 'galeria', submenu: [
        {name: 'zdjęcia' , url: 'galeria'},
        {name: 'hodowla' , url: 'galeria'}
      ]},
      {name: 'galeria', url: 'gallery'},
      {name: 'psy', url: 'lista'},
      {name: 'kontakt', url: 'contact'}
    ];
  }
}
