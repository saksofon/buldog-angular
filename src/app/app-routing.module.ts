import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ContactComponent} from './views/contact/contact.component';
import {DogListComponent} from './views/dog-list/dog-list.component';
import {MainComponent} from './views/main/main.component';
import {GalleryComponent} from './views/gallery/gallery.component';

const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: MainComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'lista', component: DogListComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'lapa', component: MainComponent },
  { path: '**', redirectTo: 'main' },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
