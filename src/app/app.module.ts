import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { MainComponent } from './views/main/main.component';
import { FooterComponent } from './core/footer/footer.component';
import { AnimalItemComponent } from './shared/animal-item/animal-item.component';
import { MenuComponent } from './shared/menu/menu.component';
import { FormComponent } from './shared/form/form.component';
import { ContactComponent } from './views/contact/contact.component';
import { DogListComponent } from './views/dog-list/dog-list.component';
import { AppRoutingModule } from './/app-routing.module';
import { GalleryComponent } from './views/gallery/gallery.component';
import { CarouselComponent } from './shared/carousel/carousel.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    AnimalItemComponent,
    MenuComponent,
    FormComponent,
    ContactComponent,
    DogListComponent,
    GalleryComponent,
    CarouselComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
