import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  public items: any[];
  constructor() { }

  ngOnInit() {
    this.items = [
      { url: 'gallery', link: '././assets/img/kori.jpg'},
      { url: 'gallery',  link: '././assets/img/kori.jpg'},
      { url: 'gallery', link: '././assets/img/kori.jpg'}
  ];
  }

}
